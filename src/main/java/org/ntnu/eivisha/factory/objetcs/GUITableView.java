package org.ntnu.eivisha.factory.objetcs;

import javafx.scene.Node;
import javafx.scene.control.TableView;

public class GUITableView extends GUIObject{

    @Override
    Node createObject() {
        return new TableView<>();
    }
}
