package org.ntnu.eivisha.controller.helpers;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import org.ntnu.eivisha.App;
import org.ntnu.eivisha.model.Patient;
import org.ntnu.eivisha.model.PatientRegister;

import java.util.ArrayList;

/**
 * Class that handles all gui elements for adding or editing a patient
 * Also checks that information is correct
 */

public class PatientDialog extends Dialog<PatientRegister> {

    public enum Mode {
        NEW, EDIT, INFO
    }

    private final Mode mode;
    private Patient existingPatient = null;

    /**
     * Constructor for TaskDialog
     */
    public PatientDialog() {
        super();
        this.mode = Mode.NEW;
        // Create the content of the dialog
        createPatient();
    }

    /**
     * Second constructor for TaskDialog
     * @param patient Patient to be edited
     * @param editable If the patient should be editable or not
     */
    public PatientDialog(Patient patient, boolean editable) {
        super();
        if (editable) {
            this.mode = Mode.EDIT;
        } else {
            this.mode = Mode.INFO;
        }
        this.existingPatient = patient;
        // Create the content of the dialog
        createPatient();
    }

    /**
     * Creates the patient input fields and then adds information to the register
     * Also checks that all information is in correct format and length
     */

    public void createPatient(){
        switch (this.mode){
            case NEW:
                setTitle("Patient Details - Add");
                break;
            case EDIT:
                setTitle("Patient Details - Edit");
                break;
            case INFO:
                setTitle("Patient Details");
                break;
            default:
                setTitle("Task Details - UNKNOWN MODE...");
                break;
        }

        getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
        getDialogPane().lookupButton(ButtonType.OK).setDisable(true);

        HBox vBox = new HBox();
        VBox details = new VBox();
        VBox inputDetails = new VBox();

        TextField firstName = new TextField();
        firstName.setPromptText("Add patient first name");

        TextField lastName = new TextField();
        lastName.setPromptText("Add patient last name");

        TextField socialSecurityNumber= new TextField();
        socialSecurityNumber.setPromptText("Add patient social security number");

        if(mode==Mode.EDIT){
            getDialogPane().lookupButton(ButtonType.OK).setDisable(false);
        }

        socialSecurityNumber.focusedProperty().addListener((arg0, oldValue, newValue) -> {
            if (!newValue) {//When focus of the TextField is lost
                if(App.getRegister().getPatients().containsKey(socialSecurityNumber.getText())&&mode==Mode.NEW){
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setContentText("Change the social security number or delete previous entry");
                    alert.setHeaderText("Patient with same social security number already registered");
                    getDialogPane().lookupButton(ButtonType.OK).setDisable(true);
                    alert.showAndWait();
                }
                else if (!socialSecurityNumber.getText().matches("[0-9]+") || !(socialSecurityNumber.getText().length()==11)) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setContentText("Social security number must be a number of length 11");
                    alert.setHeaderText("Wrong social security number");
                    alert.showAndWait();
                    getDialogPane().lookupButton(ButtonType.OK).setDisable(true);
                }
                else if(!firstName.getText().equals("")&&!socialSecurityNumber.getText().equals("")){
                    getDialogPane().lookupButton(ButtonType.OK).setDisable(false);
                }
            }
        });
        firstName.focusedProperty().addListener((arg0, oldValue, newValue) -> {
            if (!newValue) {
                if(!firstName.getText().equals("")&&!socialSecurityNumber.getText().equals("")){
                    getDialogPane().lookupButton(ButtonType.OK).setDisable(false);
                }
            }
        });

        TextField generalPractitioner= new TextField();
        generalPractitioner.setPromptText("Add general practiioner");

        TextField diagnosis= new TextField();
        diagnosis.setPromptText("Add diagnosis");


        inputDetails.getChildren().add(firstName);
        inputDetails.getChildren().add(lastName);
        inputDetails.getChildren().add(socialSecurityNumber);
        inputDetails.getChildren().add(generalPractitioner);
        inputDetails.getChildren().add(diagnosis);



        Label firstNameLabel = new Label("First name: ");
        Label lastNameLabel = new Label("Last name: ");
        Label socialSecurityNumberLabel = new Label("Social security number: ");
        Label generalPractitionerLabel = new Label("General practitioner: ");
        Label diagnosisLabel = new Label("Diagnosis: ");

        ArrayList<Separator> separators = new ArrayList<>();
        for(int i = 0;i<5;i++){
            Separator separator = new Separator(); separator.setVisible(false);
            separator.setPrefHeight(10); separator.setMinHeight(10); separator.setMaxHeight(10);
            separators.add(separator);
        }


        details.getChildren().add(firstNameLabel);details.getChildren().add(separators.get(0));
        details.getChildren().add(lastNameLabel);details.getChildren().add(separators.get(1));
        details.getChildren().add(socialSecurityNumberLabel);details.getChildren().add(separators.get(2));
        details.getChildren().add(generalPractitionerLabel);details.getChildren().add(separators.get(3));
        details.getChildren().add(diagnosisLabel);details.getChildren().add(separators.get(4));

        if (mode == Mode.INFO || mode == Mode.EDIT){
            firstName.setText(existingPatient.getFirstName());
            lastName.setText(existingPatient.getLastName());
            socialSecurityNumber.setText(existingPatient.getSocialSecurityNumber());
            generalPractitioner.setText(existingPatient.getGeneralPracticioner());
            diagnosis.setText(existingPatient.getDiagnosis());

        }

        Separator separator = new Separator();
        separator.setVisible(false);
        vBox.getChildren().add(details);
        vBox.getChildren().add(separator);
        vBox.getChildren().add(inputDetails);

        getDialogPane().setContent(vBox);
        setResultConverter((ButtonType button)->{
            PatientRegister result = App.getRegister();
            if (button == ButtonType.OK){
                if (mode == Mode.NEW){
                    Patient patient = new Patient(socialSecurityNumber.getText(), firstName.getText(), lastName.getText(), diagnosis.getText(),generalPractitioner.getText());
                    result.getPatients().containsKey(socialSecurityNumber.getText());
                    result.addPatient(patient);
                    return result;
                }
                else if(mode == Mode.EDIT){
                    Patient patient = new Patient(socialSecurityNumber.getText(), firstName.getText(), lastName.getText(), diagnosis.getText(),generalPractitioner.getText());
                    result.editPatient(existingPatient.getSocialSecurityNumber(),patient);
                }
            }
            return result;
        });
    }
}
