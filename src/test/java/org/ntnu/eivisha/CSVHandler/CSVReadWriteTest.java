package org.ntnu.eivisha.CSVHandler;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.ntnu.eivisha.App;
import org.ntnu.eivisha.model.Patient;
import org.ntnu.eivisha.model.PatientRegister;

import java.io.File;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class CSVReadWriteTest {

    PatientRegister register = new PatientRegister("Test");

    @BeforeEach
    private void generateRegister(){
        Patient patient = new Patient("12345678912","Test1","Heisann","Sykdom","Legen");
        Patient patient1 = new Patient("12345678915","Test2","Heisann","Sykdom1","Lege");
        Patient patient2 = new Patient("12345678916","Test3","Heisann","Sykdom2","Legene");
        register.addPatient(patient); register.addPatient(patient1); register.addPatient(patient2);
    }

    @Test
    void writeCSV(){
        File file = new File("src/test/java/org/ntnu/eivisha/testData/test.csv");
        CSVWriter writer = new CSVWriter(register,file);
        writer.writeCSV();
        assert (file.exists());
    }

    @Test
    void readCSV() {
        File file = new File("src/test/java/org/ntnu/eivisha/testData/test.csv");
        CSVReader reader = new CSVReader(file, App.getRegister());
        PatientRegister ogRegister = register; ArrayList ogList = new ArrayList(ogRegister.getPatients().values());
        PatientRegister newregister = reader.readCSV(); ArrayList newlist = new ArrayList(newregister.getPatients().values());

        for(int i=0; i<newregister.getPatients().size();i++){
            assert (ogList.get(i).equals(newlist.get(i)));
        }
    }
}