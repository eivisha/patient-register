package org.ntnu.eivisha.model;

import java.util.HashMap;

public class PatientRegister {

    private String name;
    private HashMap<String, Patient> patients = new HashMap<>();

    public PatientRegister(String name) {
        this.name =name;
    }

    public void addPatient(Patient patient){
        if(!patients.containsKey(patient.getSocialSecurityNumber())){
            patients.put(patient.getSocialSecurityNumber(), patient);
        }else {
            throw new IllegalArgumentException("Already registered");
        }
    }

    public void removePatient(String socialSecurityNumber){
        if (patients.containsKey(socialSecurityNumber)){
            patients.remove(socialSecurityNumber);
        }
        else{
            throw new IllegalArgumentException("Patient doesnt exist");
        }
    }

    public void removeAllPatients(){
        patients=new HashMap<>();
    }

    public void editPatient(String socialSecurityNumber, Patient newPatient){
        if (!patients.containsKey(socialSecurityNumber)){
            throw new IllegalArgumentException("Patient doesnt exist");
        }
        patients.remove(socialSecurityNumber);
        patients.put(newPatient.getSocialSecurityNumber(),newPatient);
    }

    public HashMap<String, Patient> getPatients() {
        return patients;
    }

    public void setPatients(HashMap<String, Patient> patients) {
        this.patients = patients;
    }
}
