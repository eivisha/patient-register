package org.ntnu.eivisha;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import org.ntnu.eivisha.model.PatientRegister;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Optional;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;
    private static PatientRegister register = new PatientRegister("Sykehus");

    /**
     * method ran at the launch of the application
     * Adds images and configurates window
     * @param stage The main stage
     * @throws IOException If main fxml is not found
     */

    @Override
    public void start(Stage stage) throws IOException {
        Image iconImage = new Image(new FileInputStream("src/main/resources/org/ntnu/eivisha/images/icon.png"));
        scene = new Scene((loadFXML("main")), 640, 480);
        stage.setScene(scene);
        stage.setTitle("Patient Register");
        stage.setMaxHeight(480);stage.setMaxWidth(640);
        stage.setMinHeight(480);stage.setMinWidth(640);
        stage.setHeight(480);stage.setWidth(640);
        stage.getIcons().add(iconImage);
        try {
            stage.setOnCloseRequest(event -> {
                alertOnExit();
                event.consume();
            });
        }catch (IllegalStateException e){

        }

        stage.show();
    }

    /**
     * Loads the fxml file
     * @param fxml filename of fxml
     * @return A parent object
     * @throws IOException If fxml is not found
     */
    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    /**
     * Get method for register
     * @return The register
     */

    public static PatientRegister getRegister() {
        return register;
    }

    /**
     * Sets the register
     * @param register Patient register
     */

    public static void setRegister(PatientRegister register) {
        App.register = register;
    }

    /**
     * Main method of the app
     * @param args
     */

    public static void main(String[] args) {
        launch();
    }

    /**
     * Alerts the user on exit, and checks if they want to close
     */

    public static void alertOnExit(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setContentText("Click OK to close, or cancel to cancel operation");
        alert.setHeaderText("Are you sure you want to close?");
        alert.setTitle("Closing");
        try{
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get()==ButtonType.OK) {
                Platform.exit();
            }
        }catch (IllegalStateException e){

        }


    }
}