package org.ntnu.eivisha.factory;

import org.ntnu.eivisha.factory.objetcs.GUIMenuBar;
import org.ntnu.eivisha.factory.objetcs.GUIObject;
import org.ntnu.eivisha.factory.objetcs.GUITableView;
import org.ntnu.eivisha.factory.objetcs.GUIToolBar;

public class GUIFactory {

    /**
     * Returns a node based on the input
     * @param object A string
     * @return A node
     */

    public static GUIObject getGUIObject(String object){
        switch (object){
            case "MenuBar":
                return new GUIMenuBar();
            case "ToolBar":
                return new GUIToolBar();
            case "TableView":
                return new GUITableView();
            default:
                return null;
        }
    }
}
