package org.ntnu.eivisha.factory.objetcs;

import javafx.scene.Node;

public abstract class GUIObject{
    abstract Node createObject();
}
