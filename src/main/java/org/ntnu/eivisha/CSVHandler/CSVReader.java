package org.ntnu.eivisha.CSVHandler;

import javafx.scene.control.Alert;
import org.ntnu.eivisha.App;
import org.ntnu.eivisha.model.Patient;
import org.ntnu.eivisha.model.PatientRegister;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class CSVReader {
    File file;
    String line = "";
    PatientRegister register;

    public CSVReader(File file, PatientRegister register){
        this.file=file;
        this.register = register;
    }

    /**
     * Reads the csv by using a buffered reader.
     * Reads the whole line and puts that in a string, based of the lenght it determines if
     * a diagnosis is present. Then creates a patient from the string and adds it to the register
     */

    public PatientRegister readCSV() {
        BufferedReader bufferedReader = null;

        String faults="";
        int numberOfFaults=0;
        try {
            bufferedReader = new BufferedReader(new FileReader(file));
            System.out.println(bufferedReader.readLine());
            Patient patient=null;
            while((line=bufferedReader.readLine()) !=null){
               String[] strings = line.split(";");
               try{
                   if (strings.length == 5){
                       patient = new Patient(strings[3],strings[0],strings[1],strings[4],strings[2]);
                       register.addPatient(patient);
                   }else{
                       patient = new Patient(strings[3],strings[0],strings[1],"",strings[2]);
                       register.addPatient(patient);
                   }
               }catch (IllegalArgumentException e){
                   faults+=patient.getFirstName()+" "+ patient.getLastName()+", ";
                   numberOfFaults+=1;
               }
           }
        }catch (IOException e){
            System.out.println(e.getMessage());
        } finally {
            try {
                bufferedReader.close();
            }catch (IOException e){
                System.out.println(e.getMessage());
            }
        }
        if (faults.length()>1){
            if (faults.length()>200){
                faults=faults.substring(0,200)+"...";
            }
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Info");
            alert.setHeaderText("Multiple registered already or formatted incorrectly");
            alert.setContentText("The following has not been registered: \n"+faults+"\nTotal not registered: "+numberOfFaults);
            alert.showAndWait();
        }
        else if (faults.length()==1){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Info");
            alert.setHeaderText("Patient registered already or formatted incorrectly");
            alert.setContentText(faults+" has not been registered");
            alert.showAndWait();
        }
        return register;
    }
}
