package org.ntnu.eivisha.factory.objetcs;

import javafx.scene.Node;
import javafx.scene.control.MenuBar;

public class GUIMenuBar extends GUIObject{

    @Override
    public Node createObject() {
        return new MenuBar();
    }
}
