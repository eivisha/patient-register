package org.ntnu.eivisha.CSVHandler;

import org.ntnu.eivisha.model.Patient;
import org.ntnu.eivisha.model.PatientRegister;

import java.io.*;
import java.util.ArrayList;

public class CSVWriter {
    PatientRegister register;
    String path;
    File file;

    public CSVWriter(PatientRegister register, File file){
        this.register=register;
        this.path=file.getAbsolutePath();
        this.file=file;
    }

    public void writeCSV() {
        if (!file.getAbsolutePath().equals(null)){
            ArrayList<Patient> patients = new ArrayList<>(register.getPatients().values());
            FileWriter fileWriter = null;
            BufferedWriter bufferedWriter = null;
            PrintWriter printWriter = null;
            try{
                fileWriter = new FileWriter(file.getAbsolutePath());
                bufferedWriter = new BufferedWriter(fileWriter);
                printWriter = new PrintWriter(bufferedWriter);
                printWriter.write("firstName;lastNam;generalPractitioner;socialSecurityNumber\n");
                for(Patient p: patients){
                    if (p.getDiagnosis().equals("")){
                        String string = p.getFirstName()+ ";"+
                                p.getLastName()+";"+
                                p.getGeneralPracticioner()+";"+
                                p.getSocialSecurityNumber();
                        printWriter.println(string);
                    }else {
                        String string = p.getFirstName()+ ";"+
                                p.getLastName()+";"+
                                p.getGeneralPracticioner()+";"+
                                p.getSocialSecurityNumber()+";"+
                                p.getDiagnosis();
                        printWriter.println(string);
                    }
                }
            }catch (IOException e){
                System.out.println(e.getMessage());
            }finally {
                try {
                    printWriter.close();
                    bufferedWriter.close();
                    fileWriter.close();
                }catch (Exception i){
                    System.out.println(i.getMessage());
                }
            }
        }
    }
}
