package org.ntnu.eivisha.model;

import java.util.Objects;

public class Patient {

    private String socialSecurityNumber;
    private String firstName;
    private String lastName;
    private String diagnosis;
    private String generalPracticioner;

    public Patient(String socialSecurityNumber, String firstName, String lastName, String diagnosis, String generalPracticioner) {
        if (socialSecurityNumber.length()==11){
            this.socialSecurityNumber = socialSecurityNumber;
        }else { throw new IllegalArgumentException("Not correct length");}
        this.firstName = firstName;
        this.lastName = lastName;
        this.diagnosis = diagnosis;
        this.generalPracticioner = generalPracticioner;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public String getGeneralPracticioner() {
        return generalPracticioner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patient patient = (Patient) o;
        return Objects.equals(socialSecurityNumber, patient.socialSecurityNumber);
    }

    @Override
    public String toString() {
        return "Patient{" +
                "socialSecurityNumber='" + socialSecurityNumber + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", diagnosis='" + diagnosis + '\'' +
                ", generalPracticioner='" + generalPracticioner + '\'' +
                '}';
    }
}
