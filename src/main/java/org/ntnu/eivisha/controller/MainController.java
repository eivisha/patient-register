package org.ntnu.eivisha.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.ntnu.eivisha.App;
import org.ntnu.eivisha.CSVHandler.CSVReader;
import org.ntnu.eivisha.CSVHandler.CSVWriter;
import org.ntnu.eivisha.controller.helpers.RegisterModifier;
import org.ntnu.eivisha.model.Patient;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Main controller. All fxml related methods reside here
 */
public class MainController {

    public TableView<Patient> tableView;
    public Button addButton;
    public Button removeButton;
    public Button editButton;
    public TableColumn<Patient, String> socialSecurityNumberColumn;
    public TableColumn<Patient, String> lastNameColumn;
    public TableColumn<Patient, String> firstNameColumn;
    public TableColumn<Patient, String> generalPractitioner;
    public TableColumn<Patient, String> diagnosis;
    public MenuItem aboutButton;
    public Separator separator;

    public void initialize() throws FileNotFoundException {
        ImageView plussImage = new ImageView(new Image(new FileInputStream("src/main/resources/org/ntnu/eivisha/images/pluss.png")));
        ImageView minusImage = new ImageView(new Image(new FileInputStream("src/main/resources/org/ntnu/eivisha/images/minus.png")));
        ImageView editImage = new ImageView(new Image(new FileInputStream("src/main/resources/org/ntnu/eivisha/images/edit.png")));

        plussImage.setFitHeight(50); plussImage.setPreserveRatio(true);
        addButton.setGraphic(plussImage);
        minusImage.setFitHeight(50); minusImage.setPreserveRatio(true);
        removeButton.setGraphic(minusImage);
        editImage.setFitHeight(50); editImage.setPreserveRatio(true);
        editButton.setGraphic(editImage);
        separator.setVisible(false);

        columnFactory();
        update();
        aboutButton.setOnAction(showAbout());
    }

    /**
     * Adds a patient through RegisterModifier
     */

    @FXML
    public void addPatient(){
        RegisterModifier.addPatient();
        update();
    }

    /**
     * Remove a patient through RegisterModifier
     */

    @FXML
    public void removePatient(){
        RegisterModifier.removePatient(tableView.getSelectionModel().getSelectedItem());
        update();
    }

    /**
     * Edits a patient through the Register modifier
     */

    @FXML
    public void editPatient(){
        RegisterModifier.editPatient(tableView.getSelectionModel().getSelectedItem());
        update();
    }

    public void removeAllPatients() {
        RegisterModifier.removeAll();
        update();
    }

    /**
     * Method for importing csv
     * Checks if file is imported properly and
     * Makes sure that a correct file is loaded
     */

    @FXML
    public void importCSV() {
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose a file");
        File file = fileChooser.showOpenDialog(stage);
        CSVReader reader = new CSVReader(file,App.getRegister());
        try {
            while (!file.toString().contains(".csv")) {
                Alert alert = new Alert(AlertType.CONFIRMATION);
                alert.setTitle("Error");
                alert.setHeaderText("Chosen file is not .csv");
                alert.setContentText("Press OK to choose again, or cancel to cancel operation");
                Optional<ButtonType> checkForWrongFileType = alert.showAndWait();
                if (checkForWrongFileType.get() == ButtonType.OK) {
                    stage = new Stage();
                    stage.initModality(Modality.APPLICATION_MODAL);
                    fileChooser = new FileChooser();
                    fileChooser.setTitle("Choose a file");
                    file = fileChooser.showOpenDialog(stage);
                    reader = new CSVReader(file,App.getRegister());
                }
                else if(checkForWrongFileType.get() == ButtonType.CANCEL){
                    return;
                }
            }
            App.setRegister(reader.readCSV());
            update();
        }catch (NullPointerException e){
            System.out.println(e.getMessage());
        }
    }

    /**
     * Exports the register as a CSV file Checks first if register is empty or not
     */

    @FXML
    public void exportCSV(){
        if (App.getRegister().getPatients().size()==0){
            Alert alert = new Alert(AlertType.ERROR);
            alert.setContentText("Add some patients and try again");
            alert.setTitle("Error");
            alert.setHeaderText("You cant export an empty register");
            alert.showAndWait();
        }else{
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Choose a location to save file");
            fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter(".csv","*.csv"));
            try{
                File file = fileChooser.showSaveDialog(stage);
                CSVWriter csvWriter = new CSVWriter(App.getRegister(), file);
                csvWriter.writeCSV();
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Success");
                alert.setContentText("Saved to "+file.getAbsolutePath());
                alert.setHeaderText("Register was saved");
                alert.showAndWait();
            }catch (NullPointerException e){
                System.out.println(e.getMessage());
            }

        }
    }

    public void exit() {
        App.alertOnExit();
    }


    /**
     * Sets values of columns
     */

    private void columnFactory(){
        socialSecurityNumberColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        diagnosis.setCellValueFactory(new PropertyValueFactory<>("diagnosis"));
        generalPractitioner.setCellValueFactory(new PropertyValueFactory<>("generalPracticioner"));
    }

    /**
     * Updates the tableview
     */

    private void update(){
        ObservableList<Patient> list;
        list = FXCollections.observableList(App.getRegister().getPatients().values().stream().collect(Collectors.toList()));
        tableView.setItems(list);
        tableView.refresh();
    }

    /**
     * Shows about window
     * @return An eventHandler
     */

    private EventHandler<ActionEvent> showAbout() throws FileNotFoundException {
        ImageView iconImage = new ImageView(new Image(new FileInputStream("src/main/resources/org/ntnu/eivisha/images/icon.png")));
        iconImage.setFitHeight(100); iconImage.setFitWidth(100);
        return event -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            DialogPane dialogPane = alert.getDialogPane();
            alert.setDialogPane(dialogPane);
            alert.setGraphic(iconImage);
            alert.initModality(Modality.APPLICATION_MODAL);
            alert.setTitle("About");
            alert.setHeaderText("Patient Register");
            alert.setContentText("Version 1.0\n" +
                    "Made by Eivind Strand Harboe" +
                    "\n2021 \u00A9");
            alert.showAndWait();
        };
    }
}