package org.ntnu.eivisha.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class PatientRegisterTest {

    PatientRegister register = new PatientRegister("Sykehus");

    @BeforeEach
    private void createRegister(){
        Patient patient0 = new Patient("14040800772","Andreas","Starnd","Syk","Helle Løken");
        register.addPatient(patient0);
        Patient patient1 = new Patient("19040800772","Arne","Starnd","Syk","Helle Løken");
        register.addPatient(patient1);
    }

    @Test
    public void removePatientPositive(){
        String scn = "19040800772";
        register.removePatient(scn);
        assert (!register.getPatients().containsKey(scn));
    }

    @Test
    public void removePatientNegative(){
        String scn = "1404080067274";
        assertThrows(IllegalArgumentException.class, () ->{
            register.removePatient(scn);
        });
        assert (register.getPatients().containsKey("14040800772"));
    }

    @Test
    public void addPatientPositive(){
        String scn = "07040600772";
        Patient patient = new Patient(scn,"Per","Starnd","Syk","Helle Løken");
        register.addPatient(patient);
        assert (register.getPatients().containsKey(scn));
    }

    @Test
    public void editPatient(){
        Patient patient1 = new Patient("28030845028","Helge","Martinsen","Syk","Hanne Løken");
        register.editPatient("19040800772",patient1);
        assert (register.getPatients().containsKey("28030845028"));
    }

    @Test
    public void removeAllPatients(){
        register.removeAllPatients();
        assert (register.getPatients().size()==0);
    }
}