package org.ntnu.eivisha.controller.helpers;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import org.ntnu.eivisha.App;
import org.ntnu.eivisha.model.Patient;
import org.ntnu.eivisha.model.PatientRegister;

import java.util.Optional;

public class RegisterModifier {

    /**
     * Adds a patient to the register
     */

    public static void addPatient(){
        PatientDialog patientDialog = new PatientDialog();
        Optional<PatientRegister> result = patientDialog.showAndWait();
        if (result.isPresent()){
            PatientRegister register = result.get();
        }
    }

    /**
     * Edits selected patient
     * @param existingPatient The patient to be edited
     */

    public static void editPatient(Patient existingPatient){
        if(existingPatient == null){
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Select a Patient:");
            alert.setHeaderText("No patient selected" );
            alert.setContentText("Please select a patient to edit");
            alert.showAndWait();
        }else{
            PatientDialog dialog = new PatientDialog(existingPatient, true);
            Optional<PatientRegister> result = dialog.showAndWait();
            if (result.isPresent()){
                PatientRegister updatedRegister = result.get();
                App.setRegister(updatedRegister);
            }
        }
    }

    /**
     * Removes a patient from the register
     * @param patient Patient to be removed
     */

    public static void removePatient(Patient patient){
        if (patient == null){
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Select a Patient:");
            alert.setHeaderText("No patient selected");
            alert.setContentText("Please select a patient to delete");
            alert.showAndWait();
        }else{
            PatientRegister register = App.getRegister();
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Remove patient");
            alert.setContentText("Are you sure you want to remove this patient");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK){
                register.removePatient(patient.getSocialSecurityNumber());
                App.setRegister(register);
            }
        }
    }

    /**
     * Removes all patients
     */

    public static void removeAll(){

        PatientRegister register = App.getRegister();
        if (register.getPatients().size() == 0){
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("No patients in register");
            alert.setContentText("Add some patients and try again");
            alert.showAndWait();
        }
        else {
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Remove patient");
            alert.setHeaderText("Are you sure you want to remove all patients");
            alert.setContentText("Press ok to remove all or cancel to cancel operation");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK){
                register.removeAllPatients();
                App.setRegister(register);
            }
        }
    }
}