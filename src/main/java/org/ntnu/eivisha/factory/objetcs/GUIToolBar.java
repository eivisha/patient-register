package org.ntnu.eivisha.factory.objetcs;

import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ToolBar;
import javafx.scene.image.ImageView;

/**
 * Toolbar object used in factory
 */

public class GUIToolBar extends GUIObject{

    /**
     * Creates a toolbar
     * @return A node
     */

    @Override
    public Node createObject() {
        ToolBar toolBar = new ToolBar();

        Button plusButton = new Button();
        plusButton.setGraphic(new ImageView("org/ntnu/eivisha/images/pluss.png"));

        Button minusButton = new Button();
        minusButton.setGraphic(new ImageView("org/ntnu/eivisha/images/minus.png"));

        Button editButton = new Button();
        editButton.setGraphic(new ImageView("org/ntnu/eivisha/images/edit.png"));

        toolBar.getItems().add(plusButton);
        toolBar.getItems().add(minusButton);
        toolBar.getItems().add(editButton);

        return toolBar;
    }
}